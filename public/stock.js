var ready = (callback) => {
  if (document.readyState != "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => { 
  document.getElementById("submit").addEventListener('click',handleClick)
});

function handleClick(event) {
  bootbox.confirm({
    title: 'Continue?',
    message: 'Do you wanna continue?',
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> No',
        className: 'btn-danger'
      },
      confirm: {
        label: '<i class="fa fa-check"></i> Yes',
        className: 'btn-info'
      }
    },
    callback: function(result) {
      if (result) {
        bootbox.alert('Form Submitted');
      } else {
        bootbox.alert('Action Cancelled');
      }
    }
  });
}

  
function makeplot(stock_file) {
    console.log("makeplot: start")
    fetch(stock_file)
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  
};


function processData(data) {
  console.log("processData: start")
  let x = [], y = []

  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("processData: end")
}

function makePlotly(x, y) {
    console.log("makePlotly: start");
    let ID = document.getElementById("stock-select");
    let stockchoice = ID.options[ID.selectedIndex].value;
    let title = `${getTitle(stockchoice)}`;
  
    var traces = [{
        x: x,
        y: y
      }];
    let layout = { title };
    stockgraph = document.getElementById('stockgraph');
  
    Plotly.newPlot( stockgraph, traces, layout );
    console.log("makePlotly: end");
  }
  
  function getTitle(stockchoice) {
    switch (stockchoice) {
      case "AAPL":
        return "Apple Stock Price History";
      case "GME":
        return "GameStop Stock Price History";
      case "SPY":
        return "S & P 500 Stock Price History";
      case "TSLA":
        return "Tesla Stock Price History";
      default:
        return "Stock Price History";
    }
  }
  
function findplot() {
    let ID = document.getElementById("stock-select");
    let stockchoice = ID.options[ID.selectedIndex].value;
    let fileName = `data/${stockchoice}.csv`;
    makeplot(fileName);
  }
  